

/* ----------------------------------------------------------------------- */

/* wordcount.c - this code must be linked with the 'AssemblerPatch.o' 
                 68000 patch */

#define  FIB_SIZE             (LONG) (sizeof(struct FileInfoBlock))

#include <exec/types.h>
#include <exec/memory.h>
#include <libraries/dos.h>

ULONG   g_filesize; /* exported as _g_filesize  */

ULONG   g_word_count; /* exported as _word_count */

ULONG   g_line_count; /* exported as _line_count */
 
ULONG   g_buffer_p; /* exported as _g_buffer_p */

main(int argc, char *argv[])

{

BOOL LoadBuffer(), error_flag=TRUE; /* guilty until proven innocent approach */

struct FileHandle *fh, *Open();

struct FileLock *filelock_p;

struct FileInfoBlock *fib_p;


if(argc=2)
        
 { /* user has given a filename so let's see if it exists */

   if (filelock_p=(struct FileLock *)Lock(argv[1],ACCESS_READ))

   {

       if(fib_p=(struct FileInfoBlock *)AllocMem(FIB_SIZE,MEMF_PUBLIC))

     {

      if(Examine(filelock_p,fib_p))

      {

       g_filesize=fib_p->fib_Size; /* now we know how big the file is */

       UnLock(filelock_p);FreeMem(fib_p,FIB_SIZE);

       if(g_buffer_p=AllocMem(g_filesize,MEMF_PUBLIC))

        {

         if((fh=Open(argv[1], MODE_OLDFILE))!=NULL) 

                {

                 error_flag=FALSE; /* clear 'cause everything worked O.K. */
                 g_filesize=Read(fh,g_buffer_p,g_filesize);

                 Close(fh);

                 WordCount(); /* do our assembler stuff */

                 printf("word count = %d  \n", g_word_count);

                }

          FreeMem(g_buffer_p, g_filesize);

         } /* end of if AllocMem() for buffer */

       } /* end of if Examine() */

     } /* end of if (fib_p */

   } /* end of if (filelock */

 } /* end of if(argc=2) */

 if (error_flag) {printf("sorry - cannot get details \n");}

}/* bye bye to  main() */

/* ----------------------------------------------------------------------- */




