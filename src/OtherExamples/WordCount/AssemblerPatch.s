         opt L+
* ===================================================================== * 
* AUI-SPELL PATCH: word_count.s for version 0.10                        *
*                                                                       *
* Programmer:      Paul Overaa                                          *
*                                                                       *
* Date:            1st March 91                                         *
*                                                                       *
* Patch for analysing an ASCII file and counting words and linefeeds.   *
* --------------------------------------------------------------------- *

* a0    is loaded with the address of the start of the buffer
* a1    holds the start of the current word

* d4    holds the character count of the current word   
* d5    is loaded with  the total number of characters in the file

         XDEF    _WordCount

         XREF   _g_buffer_p
         XREF   _g_filesize
         XREF   _g_line_count
         XREF   _g_word_count
         
lowercase_z   equ   $7A
lowercase_a   equ   $61
uppercase_z   equ   $5A
uppercase_a   equ   $41
LINEFEED      equ   $0A

* --------------------------------------------------------------------- *


_WordCount         movem.l a2-a6/d2-d7,-(sp)       preserve for Lattice
         
* It's easy to get confused about the next line of code so here's a
* general note which might help................ If the C program had a 
* declaration like UBYTE g_buffer[10000] then the address of the variable 
* would be the start address of the buffer area and we'd use immediate
* addressing, i.e. move.l #_g_buffer_p,a0, to load a0. BUT.........
* since we are actually using AllocMem() to get memory we have made the 
* declaration LONG g_buffer_p, so it's the CONTENTS of g_buffer_p that 
* need to be loaded into a0, hence we use a   move.l _g_buffer_p, a0 
* instruction instead !
   
                        move.l  _g_buffer_p,a0        buffer start - see above note
                        move.l  _g_filesize,d5        characters in file
                        move.l  #0,_g_word_count      no words yet !

* --------------------------------------------------------------------- *         

FINDSTART               cmpi.b  #lowercase_z,(a0)     is char a-z ?
                        bhi     NOTLOWERCASE
                        cmpi.b  #lowercase_a,(a0)
                        bcs     NOTLOWERCASE
                        move.l  a0,a1                 put word start in a1
                        moveq   #1,d4                 initialize character count
                        bra     START_FOUND           now look for end

NOTLOWERCASE            cmpi.b  #uppercase_z,(a0)     is char A-Z ?
                        bhi     NOTLETTER
                        cmpi.b  #uppercase_a,(a0)
                        bcs     NOTLETTER
                        move.l  a0,a1                 put word start in a1
                        moveq   #1,d4                 initialize character count
                        bra     START_FOUND           now look for end

NOTLETTER               cmpi.b  #LINEFEED,(a0)        end of line char ?
                        bne     NOTLETTER1
                        addq.l  #1,_g_line_count      count line

NOTLETTER1              addq.l  #1,a0                 point to next character
                        subq.l  #1,d5                 decrease characters left count
                        bne     FINDSTART             and see if that's the word start
                        bra     FINISH

START_FOUND             addq.l  #1,_g_word_count      count this word
 
FINDEND                 addq.l  #1,a0                 point to next character
                        subq.l  #1,d5                 decrease characters left count
                        beq     FINISH                end of file found so quit
                        cmpi.b  #lowercase_z,(a0)     is char a-z ?
                        bhi     NOTLOWERCASE2
                        cmpi.b  #lowercase_a,(a0)
                        bcs     NOTLOWERCASE2
                        addq.b  #1,d4                 increment 8 bit character count
                        bra     FINDEND

NOTLOWERCASE2           cmpi.b  #uppercase_z,(a0)     is char A-Z ?
                        bhi     NOTLETTER2
                        cmpi.b  #uppercase_a,(a0)
                        bcs     NOTLETTER2
                        addq.b  #1,d4                 increment character count
                        bra     FINDEND

NOTLETTER2              cmpi.b  #LINEFEED,(a0)        end of line char ?
                        bne     NOTLETTER3
                        addq.l  #1,_g_line_count      count line

NOTLETTER3              addq.l  #1,a0                 move to next character
                        subq    #1,d5                 decrease characters left count
                        bne     FINDSTART

* --------------------------------------------------------------------- * 
FINISH                  movem.l   (sp)+,a2-a6/d2-d7   re-instate for Lattice
                        rts                           back to C

* ===================================================================== *


