* --------------------------------------------------------------------
* Example ShowRexx.s
* --------------------------------------------------------------------

; note1:  This source must be linked with amiga.lib

; note2:  If you want to use it ShowRexx from the WorkBench 
;         you must link with suitable startup code.
 
; some system include files...

         include exec/types.i

         include exec/libraries.i

         include exec/exec_lib.i

         include intuition/intuition.i

         include intuition/screens.i
    
         include rexx/modified_storage.i
    
* --------------------------------------------------------------------    
; a macro to extend LINKLIB and thus avoid the explicit use 
; of the _LVO prefixes in the function names...

CALLSYS  MACRO

         LINKLIB _LVO\1,\2
    
         ENDM

* --------------------------------------------------------------------    

; amiga.lib library external references and exports...

        XREF _CreatePort

        XREF _DeletePort

        XDEF _SysBase

; EQUate definitions...

_AbsExecBase            EQU        4

_LVOOpenWindow          EQU     -204

_LVOCloseWindow         EQU      -72

_LVOPrintIText          EQU     -216

_LVODelay               EQU     -198

_LVOSetRast             EQU     -234

_LVORefreshWindowFrame  EQU     -456

NULL                    EQU        0

TRUE                    EQU        1

QUIT                    EQU.B 'QUIT'

* --------------------------------------------------------------------
; main program code...

OPEN_DOS_LIB      move.l  _AbsExecBase,_SysBase      store Exec library base
                  lea     dos_name,a1                library name start in a1
                  moveq   #0,d0                      any version will do
                  CALLSYS OpenLibrary,_SysBase       macro (see text for details) 
                  move.l  d0,_DOSBase                store returned value
                  beq     EXIT                       test result for success

OPEN_GRAPHIC_LIB  lea     graphics_name,a1           library name start in a1
                  moveq   #0,d0                      any version will do
                  CALLSYS OpenLibrary,_SysBase       macro (see text for details) 
                  move.l  d0,_GfxBase                store returned value
                  beq     CLOSE_DOS_LIB              test result for success

OPEN_INT_LIB      lea     intuition_name,a1          library name start in a1
                  moveq   #0,d0                      any version will do
                  CALLSYS OpenLibrary,_SysBase       macro (see text for details) 
                  move.l  d0,_IntuitionBase          store returned value
                  beq     CLOSE_GRAPHIC_LIB          test result for success

OPEN_WINDOW       lea     new_window,a0              new_window base address
                  CALLSYS OpenWindow,_IntuitionBase
                  move.l  d0,window_p
                  move.l  d0,a0
                  move.l  wd_RPort(a0),rastport_p
                  beq     CLOSE_INT_LIB

OPEN_REXX_LIB     lea     rexxlib_name,a1            library name start in a1
                  moveq   #0,d0                      any version will do
                  CALLSYS OpenLibrary,_SysBase       macro (see text for details) 
                  move.l  d0,_RexxBase               store returned value
                  bne     FIND_REXX_PORT
                  lea     Text1,a3  
                  moveq   #10,d0                     x/y print co-ordinates
                  moveq   #2,d1
                  jsr     PrintMessage               no ARexx system library
                  jsr     Delay                      wait for 2 seconds
                  bra     CLOSE_WINDOW        

FIND_REXX_PORT    CALLSYS Forbid,_SysBase            halt mulit-tasking      
                  lea     rexx_port_name,a1
                  CALLSYS FindPort,_SysBase
                  move.l  d0,rexx_port               save port address
                  CALLSYS Permit,_SysBase            re-allow multi-tasking
                  tst.l   rexx_port                  did we find it?
                  bne     CREATE_PORT
                  lea     Text2,a3
                  moveq   #10,d0                     x/y print co-ordinates
                  moveq   #2,d1
                  jsr     PrintMessage               no REXX port
                  jsr     Delay                      wait for 2 seconds
                  bra     CLOSE_REXX_LIB

CREATE_PORT       move.l  #0,-(sp)                 
                  pea     my_port_name
                  jsr     _CreatePort                amiga.lib function
                  move.l  d0,my_port                 save address
                  addq.l  #8,sp                      adjust stack
                  tst.l   my_port                    is it non-zero ?
                  bne     WAIT_TO_QUIT
                  lea     Text3,a3
                  moveq   #10,d0                     x/y print co-ordinates
                  moveq   #2,d1
                  jsr     PrintMessage               cannot create port
                  jsr     Delay                      wait for 2 seconds
                  bra     CLOSE_REXX_LIB

WAIT_TO_QUIT      jsr     DisplayTitle
                  move.l  my_port,a2                 my port address
                  jsr     WaitForARexxMessage

DELETE_PORT       move.l  my_port,-(sp)
                  jsr     _DeletePort                amiga.lib fucntion
                  addq.l  #4,sp                      adjust stack     
                  
CLOSE_REXX_LIB    move.l  _RexxBase,a1               base needed in a1         
                  CALLSYS CloseLibrary,_SysBase

CLOSE_WINDOW      move.l  window_p,a0
                  CALLSYS CloseWindow,_IntuitionBase

CLOSE_INT_LIB     move.l  _IntuitionBase,a1   
                  CALLSYS CloseLibrary,_SysBase

CLOSE_GRAPHIC_LIB move.l  _GfxBase,a1           
                  CALLSYS CloseLibrary,_SysBase

CLOSE_DOS_LIB     move.l  _DOSBase,a1           
                  CALLSYS CloseLibrary,_SysBase
     
EXIT              clr.l    d0
                  rts                                logical end of program

* ====================================================================

; Function name:     DisplayTitle()
  
; Purpose:           To clear background and set up title text.

DisplayTitle         movem.l d0-d4/a0-a4,-(sp)       preserve registers

                     move.l  rastport_p,a1

                     move.l  #0,d0                   background colour
                     
                     CALLSYS SetRast,_GfxBase

                     move.l  window_p,a0             
                     
                     CALLSYS RefreshWindowFrame,_IntuitionBase

                     moveq   #10,d0                  x print position

                     moveq   #2,d1                   y line number

                     lea     title1,a3 

                     jsr     PrintMessage

                     moveq   #3,d1                   y line number

                     lea     title2,a3 

                     jsr     PrintMessage
  
                     moveq   #4,d1                   y line number

                     lea     title3,a3 

                     jsr     PrintMessage

                     movem.l   (sp)+,d0-d4/a0-a4     restore registers

                     rts

* ====================================================================
                     
; Function name:     PrintMessage()
  
; Purpose:           Used to display a string from an ARexx message  
;                    using RP_JAM2 mode. 

; Input Parameters:  a3: Load with the address of the text string 

;                    d0: Load with the x pixel print co-ordinate 

;                    d1: The line (y) position is obtained by multiplying 

;                        the pixel contents of d1 by 8. 


; Output parameters: None


; Register Usage:    a0: loaded with window's rastport address  
                     
;                    a1: loaded with IntuiText structure address

;                    a3: holds string to print

;                    d0: x print position  
                     
;                    d1: d0*8  = y print position 


; Other Notes:       All registers are preserved

* --------------------------------------------------------------------

PrintMessage      movem.l d0-d2/a0-a2,-(sp)          preserve registers

                  asl.b   #3,d1                      line (y) position
                  
                  lea     intuitext,a1               structure address

                  move.l  a3,it_IText(a1)            set string pointer
                  
                  move.l  window_p,a0                get window address

                  move.l  wd_RPort(a0),a0            get rastport address           

                  CALLSYS PrintIText,_IntuitionBase     
                 
                  movem.l   (sp)+,d0-d2/a0-a2        restore registers

                  rts
                  
* ====================================================================

; Function name:     WaitForARexxMessage()


; Purpose:           Waits until ARexx message is received, collects
;                    ARG0 ArgString pointer and passes it to the
;                    print routine. 


; Input Parameters:  Address of port should be in a2. 


; Output parameters: None


; Register Usage:    a0: Used by WaitPort() and GetMsg()  
                     
;                    a1: Used by ReplyMsg()
                     
;                    a2: Holds port address

;                    a3: Holds most recent ArgString command pointer 

;                    a4: Holds message address

;                    d0: Used by WaitPort(), GetMsg()
                     
;                    d1: holds a line number for printing

;                    d2: Used as an exit flag (quit when non-zero)

; Other Notes:       All registers are preserved

* --------------------------------------------------------------------

WaitForARexxMessage  movem.l  d0-d2/a0-a4,-(sp)      preserve registers

                     clr.l    d2                     clear exit flag

WaitForARexxMessage2 move.l   a2,a0                  port address
  
                     CALLSYS  WaitPort,_SysBase  

                     jsr      GetMessage           

                     cmpi.l   #TRUE,d2               exit flag set?

                     bne      WaitForARexxMessage2

                     movem.l   (sp)+,d0-d2/a0-a4     restore registers

                     rts                             logical end of routine

* --------------------------------------------------------------------
                     
GetMessage           move.l   a2,a0                  get port address in a0

                     CALLSYS  GetMsg,_SysBase        get the message

                     tst.l    d0

                     beq      GetMessageExit         did it exist?

                     move.l   d0,a4                  copy pointer to a4

                     jsr      DisplayTitle           

                     jsr      PrintArgument
                     
                     cmpi.l   #QUIT,(a3)             now check first argument 

                     bne      GetMessage1          

                     move.l   #TRUE,d2              
                     
GetMessage1          move.l   #0,rm_Result1(a4)      set ARexx NO ERROR flags

                     move.l   #0,rm_Result2(a4)   
                      
                     move.l   a4,a1                  message pointer
                     
                     CALLSYS  ReplyMsg,_SysBase

                     jsr      Delay                  wait for 2 seconds

                     bra      GetMessage             check for more messages

GetMessageExit       rts                             d2 holds exit flag

* --------------------------------------------------------------------

PrintArgument        moveq    #10,d0                 x print co-ordinate
                     
                     moveq    #6,d1                  y line print co-ordinate
                     
                     move.l   rm_Args(a4),a3         get ARG0 pointer

                     jsr      PrintMessage           and print in window
                                          
                     rts

* ====================================================================

Delay                moveq    #100,d1
                     
                     CALLSYS  Delay,_DOSBase         wait for 2 seconds

                     rts

* ====================================================================

; variables and static data...

_DOSBase          ds.l    1
    
_IntuitionBase    ds.l    1

_SysBase          ds.l    1

_GfxBase          ds.l    1

_RexxBase         ds.l    1

window_p          ds.l    1

rastport_p        ds.l    1

my_port           ds.l    1
 
rexx_port         ds.l    1


dos_name          dc.b 'dos.library',NULL

intuition_name    dc.b 'intuition.library',NULL

graphics_name     dc.b 'graphics.library',NULL

rexxlib_name      dc.b 'rexxsyslib.library',NULL

my_port_name      dc.b 'ShowRexx',NULL

rexx_port_name    dc.b 'REXX',NULL

        cnop 0,2

new_window

    dc.w    20,20    ;window XY origin relative to screen top left 
    dc.w    410,80    ;width and height
    dc.b    -1,-1    ; pens
    dc.l    NULL    ;IDCMP flags
    dc.l    SMART_REFRESH+WINDOWDRAG+WINDOWDEPTH+WINDOWSIZING+NOCAREREFRESH ;window flags
    dc.l    NULL    ;no gadgets 
    dc.l    NULL    ;no custom CHECKMARK imagery
    dc.l    NewWindowName    ;window title
    dc.l    NULL    ;no custom screen pointer
    dc.l    NULL    ;no custom bitmap
    dc.w    410,80    ;minimum width and height
    dc.w    -1,80     ;maximum width and height
    dc.w    WBENCHSCREEN    ;destination screen type

NewWindowName
 
  
    dc.b    'ShowRexx',NULL

    cnop 0,2


intuitext

    dc.b    1,0,RP_JAM1,0 ;front/back pens, drawmode and fill byte
    dc.w    0,0     ;XY origin relative to container TopLeft
    dc.l    NULL    ;font pointer or NULL for default
    dc.l    NULL    ;pointer to text
    dc.l    NULL    ;next IntuiText structure

Text1   dc.b    'ERROR: rexxsyslib.library not found',NULL

Text2   dc.b    'ERROR: REXX port not found',NULL

Text3   dc.b    'ERROR: could not create ARexx1 port',NULL

title1  dc.b    'Program displays all ARexx messages that are',NULL

title2  dc.b    'sent to a port named ShowRexx.  To terminate', NULL
 
title3  dc.b    'the program you must send a QUIT message....', NULL
 
* --------------------------------------------------------------------

    
