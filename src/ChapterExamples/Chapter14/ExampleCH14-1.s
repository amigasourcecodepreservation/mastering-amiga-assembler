* --------------------------------------------------------------------
* Example CH14-1.s
* --------------------------------------------------------------------
; some system include files...

         include exec/types.i

         include exec/libraries.i

         include exec/exec_lib.i

         include intuition/intuition.i

         include intuition/screens.i
    
* --------------------------------------------------------------------    
; a macro to extend LINKLIB and thus avoid the explicit use 
; of the _LVO prefixes in the function names...

CALLSYS  MACRO

         LINKLIB _LVO\1,\2
    
         ENDM

* --------------------------------------------------------------------    

; EQUate definitions...

_AbsExecBase      EQU        4

_LVOOpenWindow    EQU     -204

_LVOCloseWindow   EQU      -72

_LVOPrintIText    EQU     -216

_LVODrawBorder    EQU     -108

WIDTH             EQU      600

HEIGHT            EQU      100

DETAIL_PEN        EQU       -1

BLOCK_PEN         EQU       -1

NULL              EQU        0

TRUE              EQU        1

* --------------------------------------------------------------------
; main program code...

                  move.l  _AbsExecBase,_SysBase      store Exec library base
         
                  lea     intuition_name,a1          library name start in a1
    
                  moveq   #0,d0                      any version will do
    
                  CALLSYS OpenLibrary,_SysBase       macro (see text for details) 

                  move.l  d0,_IntuitionBase          store returned value

                  beq     EXIT                       test result for success



OPEN_WINDOW       lea     new_window,a0              new_window base address

                  CALLSYS OpenWindow,_IntuitionBase
                  
                  move.l  d0,window_p
                  
                  beq     CLOSE_LIB


PRINT_ITEXT       move.l  window_p,a1                   get window address

                  move.l  wd_RPort(a1),a0               get rastport address           

                  lea     intuitext1,a1
                  
                  move.l  #0,d0                         no additional offsets
                  
                  move.l  #0,d1

                  CALLSYS PrintIText,_IntuitionBase
                                    
PRINT_BORDER      move.l  window_p,a1                   get window address

                  move.l  wd_RPort(a1),a0               get rastport address           

                  lea     border1,a1
                  
                  move.l  #0,d0                         no additional offsets
                  
                  move.l  #0,d1

                  CALLSYS DrawBorder,_IntuitionBase
                                    

 
                  move.l  window_p,a0                   window base address
                 
                  move.l  wd_UserPort(a0),a2            user port 
                  
                  jsr WaitForExitMessage
                  

CLOSE_WINDOW      move.l  window_p,a0

                  CALLSYS CloseWindow,_IntuitionBase


; now close the library and quit...

CLOSE_LIB         move.l  _IntuitionBase,a1          base needed in a1         
    
                  CALLSYS CloseLibrary,_SysBase
    
EXIT              clr.l    d0

                  rts                                 logical end of program


* --------------------------------------------------------------------

; Function name:     WaitForExitMessage()


; Purpose:           Wait until user hits window's close gadget


; Input Parameters:  Address of IDCMP user-port should be in a2. 


; Output parameters: None


; Register Usage:    a0: Used by WaitPort() and GetMsg()  
                     
;                    a1: Used by ReplyMsg()
                     
;                    a2: Holds user-port address

;                    d0: Used by WaitPort() and GetMsg()
                     
;                    d1: Unused but possibly altered by system functions

;                    d2: Used as an exit flag (quit when non-zero)


; Other Notes:       All registers are preserved

* --------------------------------------------------------------------

WaitForExitMessage   movem.l  d0-d2/a0-a2,-(sp)    preserve registers

                     clr.l    d2                   clear exit flag

WaitForExitMessage2  move.l   a2,a0                port address
  
                     CALLSYS  WaitPort,_SysBase  

                     jsr      GetMessage           

                     cmpi.l   #TRUE,d2             exit flag set?

                     bne      WaitForExitMessage2

                     movem.l   (sp)+,d0-d2/a0-a2   restore registers

                     rts                           logical end of routine

* --------------------------------------------------------------------
                     
GetMessage           move.l   a2,a0                get port address in a0

                     CALLSYS  GetMsg,_SysBase      get the message

                     tst.l    d0

                     beq      GetMessageExit       did it exist?

                     move.l   d0,a1                copy pointer to a1

                     cmpi.l   #CLOSEWINDOW,im_Class(a1) 

                     bne      GetMessage1          

                     move.l   #TRUE,d2             user hit close gadget 

                     
GetMessage1          CALLSYS  ReplyMsg,_SysBase

                     bra      GetMessage           check for more messages

GetMessageExit       rts                           d2 holds exit flag
                     

* --------------------------------------------------------------------
; variables and static data...
    
_IntuitionBase    ds.l    1

_SysBase          ds.l    1

window_p          ds.l    1

intuition_name    dc.b 'intuition.library',0

        cnop 0,2

new_window

	dc.w	13,26	;window XY origin relative to screen top left 
	dc.w	610,115	;width and height
	dc.b	DETAIL_PEN,BLOCK_PEN	; pens
	dc.l	CLOSEWINDOW	;IDCMP flags
	dc.l	WINDOWDRAG+WINDOWDEPTH+WINDOWCLOSE+NOCAREREFRESH ;window flags
	dc.l	NULL	;no gadgets 
	dc.l	NULL	;no custom CHECKMARK imagery
	dc.l	NewWindowName	;window title
	dc.l	NULL	;no custom screen pointer
	dc.l	NULL	;no custom bitmap
	dc.w	5,5	;minimum width and height
	dc.w	-1,-1	;maximum width and height
	dc.w	WBENCHSCREEN	;destination screen type

NewWindowName

	dc.b	'This window has been opened in the WorkBench Screen',0

	cnop 0,2

border1

	dc.w	60-2,20-2	;XY origin relative to container TopLeft
	dc.b	3,0,RP_JAM1	;front pen, back pen and drawmode
	dc.b	5	;number of XY vectors
	dc.l	BorderVectors1	;pointer to XY vectors
	dc.l	NULL	;no next border

BorderVectors1

	dc.w	0,0
	dc.w	420,0
	dc.w	420,10
	dc.w	0,10
	dc.w	0,0

intuitext1

	dc.b	3,0,RP_JAM2,0	;front and back text pens, drawmode and fill byte
	dc.w	60,20	;XY origin relative to container TopLeft
	dc.l	NULL	;font pointer or NULL for default
	dc.l	ITextText1	;pointer to text
	dc.l	intuitext2	;next IntuiText structure

ITextText1

	dc.b	'The border around this was drawn using DrawBorder()',0

	cnop 0,2

intuitext2

	dc.b	3,0,RP_JAM2,0	;front and back text pens, drawmode and fill byte
	dc.w	40,40	;XY origin relative to container TopLeft
	dc.l	NULL	;font pointer or NULL for default
	dc.l	ITextText2	;pointer to text
	dc.l	intuitext3	;next IntuiText structure

ITextText2

	dc.b	'and all of the text written using PrintIText() function',0


	cnop 0,2

intuitext3

	dc.b	3,0,RP_JAM2,0	;front and back text pens, drawmode and fill byte
	dc.w	20,60	;XY origin relative to container TopLeft
	dc.l	NULL	;font pointer or NULL for default
	dc.l	ITextText3	;pointer to text
	dc.l	NULL	;next IntuiText structure


ITextText3

	dc.b	'To quit just hit the CLOSE gadget at the top left of the window',0

* --------------------------------------------------------------------

