* --------------------------------------------------------------------
* Example CH13-2.s
* --------------------------------------------------------------------
; some system include files...

         include exec/types.i

         include exec/libraries.i

         include exec/exec_lib.i

         include intuition/intuition.i

         include intuition/screens.i
    
* --------------------------------------------------------------------    
; a macro to extend LINKLIB and thus avoid the explicit use 
; of the _LVO prefixes in the function names...

CALLSYS  MACRO

         LINKLIB _LVO\1,\2
    
         ENDM

* --------------------------------------------------------------------    

; EQUate definitions...

_AbsExecBase      EQU        4

_LVOOpenWindow    EQU     -204

_LVOCloseWindow   EQU      -72

WIDTH             EQU      600

HEIGHT            EQU      100

DETAIL_PEN        EQU       -1

BLOCK_PEN         EQU       -1

TRUE              EQU        1

* --------------------------------------------------------------------
; main program code...

                  move.l  _AbsExecBase,_SysBase      store Exec library base
         
                  lea     intuition_name,a1          library name start in a1
    
                  moveq   #0,d0                      any version will do
    
                  CALLSYS OpenLibrary,_SysBase       macro (see text for details) 

                  move.l  d0,_IntuitionBase          store returned value

                  beq     EXIT                       test result for success



OPEN_WINDOW       lea     new_window,a0              new_window base address

                  move.w  #WIDTH,nw_Width(a0)

                  move.w  #HEIGHT,nw_Height(a0)

                  move.b  #DETAIL_PEN,nw_DetailPen(a0)
                  
                  move.b  #BLOCK_PEN,nw_BlockPen(a0)

                  move.l  #CLOSEWINDOW,nw_IDCMPFlags(a0)

                  move.l  #SMART_REFRESH+WINDOWDRAG+WINDOWCLOSE,nw_Flags(a0)

                  move.l  #window_title,nw_Title(a0)
                  
                  move.w  #WBENCHSCREEN,nw_Type(a0)

                  CALLSYS OpenWindow,_IntuitionBase
                  
                  move.l  d0,window_p
                  
                  beq     CLOSE_LIB


                  move.l  d0,a1                         window base address
                 
                  move.l  wd_UserPort(a1),a2            user port 
                  
                  jsr WaitForExitMessage
                  

CLOSE_WINDOW      move.l  window_p,a0

                  CALLSYS CloseWindow,_IntuitionBase


; now close the library and quit...

CLOSE_LIB         move.l  _IntuitionBase,a1          base needed in a1         
    
                  CALLSYS CloseLibrary,_SysBase
    
EXIT              clr.l    d0

                  rts                                 logical end of program


* --------------------------------------------------------------------

; Function name:     WaitForExitMessage()


; Purpose:           Wait until user hits window's close gadget


; Input Parameters:  Address of IDCMP user-port should be in a2. 


; Output parameters: None


; Register Usage:    a0: Used by WaitPort() and GetMsg()  
                     
;                    a1: Used by ReplyMsg()
                     
;                    a2: Holds user-port address

;                    d0: Used by WaitPort() and GetMsg()
                     
;                    d1: Unused but possibly altered by system functions

;                    d2: Used as an exit flag (quit when non-zero)


; Other Notes:       All registers are preserved

* --------------------------------------------------------------------

WaitForExitMessage   movem.l  d0-d2/a0-a2,-(sp)    preserve registers

                     clr.l    d2                   clear exit flag

WaitForExitMessage2  move.l   a2,a0                port address
  
                     CALLSYS  WaitPort,_SysBase  

                     jsr      GetMessage           

                     cmpi.l   #TRUE,d2             exit flag set?

                     bne      WaitForExitMessage2

                     movem.l   (sp)+,d0-d2/a0-a2   restore registers

                     rts                           logical end of routine

* --------------------------------------------------------------------
                     
GetMessage           move.l   a2,a0                get port address in a0

                     CALLSYS  GetMsg,_SysBase      get the message

                     tst.l    d0

                     beq      GetMessageExit       did it exist?

                     move.l   d0,a1                copy pointer to a1

                     cmpi.l   #CLOSEWINDOW,im_Class(a1) 

                     bne      GetMessage1          

                     move.l   #TRUE,d2             user hit close gadget 

                     
GetMessage1          CALLSYS  ReplyMsg,_SysBase

                     bra      GetMessage           check for more messages

GetMessageExit       rts                           d2 holds exit flag
                     

* --------------------------------------------------------------------
; variables and static data...
    
_IntuitionBase    ds.l    1

_SysBase          ds.l    1

window_p          ds.l    1

new_window        ds.b    nw_SIZE

window_title      dc.b 'ExampleCH13-2 test window',0

intuition_name    dc.b 'intuition.library',0

* --------------------------------------------------------------------


